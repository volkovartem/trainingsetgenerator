﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TrainingSetGenerator
{
    class Extern
    {
        #region Variables
        public static List<Thread> ListThread { get; set; }
        public static string PathToFile { get; set; }
        public static int SizeOfCard { get; set; }
        #endregion
    }
}
