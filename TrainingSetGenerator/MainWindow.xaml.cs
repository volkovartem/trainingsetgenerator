﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TrainingSetGenerator
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnProfilesPath_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog LoadProfilesFile = new Microsoft.Win32.OpenFileDialog();
            LoadProfilesFile.Filter = "Database file|*csv";
            bool? result = LoadProfilesFile.ShowDialog();
            if (result == true) lbProfilesPath.Content = LoadProfilesFile.FileName;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (System.Windows.MessageBox.Show("Do you really want to stop?", "Closing program", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No) e.Cancel = true;
            else
            {
                foreach (Thread thr in Extern.ListThread) thr.Abort();
                e.Cancel = false;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Extern.PathToFile = lbProfilesPath.Content.ToString();
                Extern.SizeOfCard = Convert.ToInt32(tbSize.Text);
                btnProfilesPath.IsEnabled = false;
                tbSize.IsEnabled = false;
                btnRun.IsEnabled = false;

                Core cr = new Core();
                Thread run = new Thread(cr.Start);
                run.Start();
                Extern.ListThread.Add(run);
            }
            catch
            {
                MessageBox.Show("Wrong settings");
            }
        }
    }
}
