﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static TrainingSetGenerator.Candles;

namespace TrainingSetGenerator
{
    class Core
    {
        public void Start()
        {
            List<Candle> InputCSV = ParsingCSV();
            if (InputCSV != null)
            {
            }
        }

        private List<Candle> ParsingCSV()
        {
            List<Candle> InputCSV = new List<Candle>();
            try
            {
                List<string> input_csv = new List<string>(File.ReadAllLines(Extern.PathToFile));

                foreach (string str in input_csv)
                {
                    string[] cndls = str.Split(',');
                    Candle cndl = new Candle
                    {
                        Hours = Convert.ToInt32(cndls[1].Split(' ')[1].Split(':')[0]),
                        Minutes = Convert.ToInt32(cndls[1].Split(' ')[1].Split(':')[1]),
                        Open = Convert.ToDouble(cndls[3]),
                        High = Convert.ToDouble(cndls[4]),
                        Low = Convert.ToDouble(cndls[5]),
                        Close = Convert.ToDouble(cndls[6]),
                        Volume = Convert.ToDouble(cndls[7])
                    };
                    InputCSV.Add(cndl);
                }
            }
            catch
            {
            }
            return InputCSV;
        }
    }
}
