﻿namespace TrainingSetGenerator
{
    class Candles
    {
        public struct Candle
        {
            public int Hours;
            public int Minutes;
            public double Open;
            public double High;
            public double Low;
            public double Close;
            public double Volume;

            public Candle(int hours, int minutes, double open, double high, double low, double close, double volume)
            {
                Hours = hours;
                Minutes = minutes;
                Open = open;
                High = high;
                Low = low;
                Close = close;
                Volume = volume;
            }
        }
    }
}
